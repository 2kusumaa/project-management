<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertOrIgnore([
            [
            'name' =>  'Administator',
            'email' => 'admin@mail.com',
            'password' => bcrypt('password'),
            'role' => 'admin',
            'initial' => 'mimin'
            ],
            ['name' =>  'Team Hero',
            'email' => 'team_hero@mail.com',
            'password' => bcrypt('password'),
            'role' => 'team',
            'initial' => 'hero'
            ],
            ['name' =>  'Andri Hartanto',
            'email' => 'andri-pm@mail.com',
            'password' => bcrypt('password'),
            'role' => 'pm',
            'initial' => 'andri'
            ]
        ]);
    }
}
