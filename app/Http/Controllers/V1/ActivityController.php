<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\ActivityModel;
use App\Entities\ProgressModel;

class ActivityController extends Controller
{
    public function store(Request $request)
    {                
        $activity = new ActivityModel;
        $data_progress = new ProgressModel;            
        
        if($request->task[0] != null){
            $tasks = $request->task[0];
                
            $pieces1 = explode(",", $tasks);            

            $activity->id_project = $request->id_project;
            $activity->activity = $request->activity;        
            $activity->deadline = $request->deadline;        
            $activity->task = json_encode($pieces1);        
            
            $activity->save();
        }               

        if($request->taskDone[0] != null){            
                        
            $taskDone = $request->taskDone[0];
           
            $pieces2 = explode(",", $taskDone);  
            
            $progress_repo = array();            
                    
            for($q=0; $q < count($pieces2); $q++) {            
                
                if($pieces2[$q] != "") {
                    
                    $param = ProgressModel::where('id_activity','=',explode("$$", $pieces2[$q])[1])
                                            ->where('task_done','=',explode("$$", $pieces2[$q])[0])
                                            ->first();
                                                        
                    
                    if($param != null){  
                        ProgressModel::where('id_activity','=',explode("$$", $pieces2[$q])[1])
                                    ->where('task_done','=',explode("$$", $pieces2[$q])[0])
                                    ->update(['status' => explode("$$", $pieces2[$q])[2]]);
                    }
                    else if($param == null){
                        array_push($progress_repo, array(
                            'id_activity' => explode("$$", $pieces2[$q])[1],
                            'task_done' => explode("$$", $pieces2[$q])[0],
                            'status' => explode("$$", $pieces2[$q])[2]
                        ));                        
                    }                   
                }                
            }
                    ProgressModel::insert($progress_repo);
        }
        
        alert()->success('Data saved', 'Successfully Add Progress!');
        return redirect()->back();        
    }

    public function destroy($id)
    {
        $project = ActivityModel::findOrFail($id);        

        $project->delete();
        alert()->success('Deleted', 'Successfully Delete Data Activity!');
        return redirect()->back();        
    }
    
}
