<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\User;
use Auth;
use Alert;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->get();
        return view('backoffice.user.index', compact('data'));
    }

    public function indexListUsers()
    {
        if(Auth::user()->role == 'admin'){
            $data_users = User::where('role', '=', 'team')
                                ->orWhere('role', '=', 'pm')
                                ->get();
            $path = "Users";
            return view('backoffice.user.index-list-users', compact('data_users','path'));
        }else{            
            alert()->warning('Not Allowed', 'You can`t access this page!');
            return redirect()->back();                     
        }        
    }

    public function storeUser(Request $request)
    {
        if(Auth::user()->role == 'admin'){
            
            $data_email = User::select('email')
                                ->where('email', $request->email)
                                ->first();                                
            $user = new User;

            if(! empty($request->file('photo'))){

                $validator = Validator::make($request->all(), [
                    'photo'   => 'mimes:jpg,jpeg,png|max:2048'
                ]);   
                
                if($validator->fails()){ 
                    alert()->error('Data not saved', 'data does not match!');
                    return redirect()->back();
                }else{                
                    $file = $request->file('photo');
                    $nama_file = rand() . '.' . $file->getClientOriginalExtension();       
                    $file->move(public_path('uploads/photo/'), $nama_file); 
                    $user->photo = $nama_file;
                }
            }
            $user->name = $request->name;
            $user->initial = $request->initial;
            
            if($data_email != null){   
                alert()->warning('Data not saved', 'email already exist!');
                return redirect()->back();                
            }
            else{
                $user->email = $request->email;
            }

            $user->password = bcrypt($request->password);
            $user->phone = $request->phone;
            $user->role = $request->role;
            
            $user->save();
            alert()->success('Data saved', 'Successfully Add Data User!');
            return redirect()->back();
        }else{
            alert()->warning('Not Allowed', 'You can`t access this feature!');            
            return redirect()->back();
        }                
    }

    public function updateUser(Request $request, $id)
    {
        if(Auth::user()->role == 'admin'){
            
            $data_email = User::select('email')
                                ->where('email', $request->email)
                                ->first();                                

            $user = User::findOrFail($id);

            if(! empty($request->file('photo'))){

                $validator = Validator::make($request->all(), [
                    'photo'   => 'mimes:jpg,jpeg,png|max:2048'
                ]);   
                
                if($validator->fails()){                
                    alert()->error('Data not saved', 'data does not match!');
                    return redirect()->back();
                }else{                
                    $file = $request->file('photo');
                    $nama_file = rand() . '.' . $file->getClientOriginalExtension();       
                    $file->move(public_path('uploads/photo/'), $nama_file); 
                    $user->photo = $nama_file;
                }
            }
            $user->name = $request->name;
            $user->initial = $request->initial;            

            if($request->email != $user->email){
                if($data_email != null){
                    alert()->warning('Data not saved', 'email already exist!');
                    return redirect()->back();
                }else{
                    $user->email = $request->email;
                }                
            }            

            $user->phone = $request->phone;
            $user->role = $request->role;
            
            $user->save();            
            alert()->success('Data saved', 'Successfully Add Data User!');
            return redirect()->back();
        }else{
            alert()->warning('Not Allowed', 'You can`t access this feature!');          
            return redirect()->back();
        }
    }

    public function destroyUser($id)
    {
        $user = User::findOrFail($id);
        
        $user->delete();
        alert()->success('Deleted', 'Data has been deleted!');            
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if(! empty($request->file('photo'))){

            $validator = Validator::make($request->all(), [
                'photo'   => 'mimes:jpg,jpeg,png|max:2048'
            ]);   
            
            if($validator->fails()){                
                alert()->error('Data not saved', 'data does not match!');
                return redirect()->back();
            }else{                
                $file = $request->file('photo');
                $nama_file = rand() . '.' . $file->getClientOriginalExtension();       
                $file->move(public_path('uploads/photo/'), $nama_file); 
                $user->photo = $nama_file;
            }
        }
        $user->name = $request->name;
        $user->email = $request->email;                

        if($request->password != null && $request->password != ""){
            $user->password = bcrypt($request->password);
        }
        $user->phone = $request->phone;
        $user->initial = $request->initial;

        $user->save();
        alert()->success('Data saved', 'Successfully Change Data!');            
        return redirect()->back();
    }

    public function admin()
    {
        return view('backoffice.dashboard.admin.index');
    }

    public function team()
    {
        return view('backoffice.dashboard.team.index');
    }
    
}
