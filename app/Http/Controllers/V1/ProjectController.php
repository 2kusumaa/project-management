<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\ProjectModel;
use App\Entities\User;
use App\Entities\ProgressModel;
use App\Entities\ActivityModel as AM;
use Auth;
use Alert;
use DB;
use Validator;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->middleware('auth');
    }

    
    public function index()
    {        
        $id = Auth::user()->id;        
        Auth::user()->role == "admin" ? $role = "admin" : $role = "team";
        $activities = AM::all();                         
        $data = ProjectModel::all();        
        $progress = ProgressModel::all();         
            
        $repoIDS = array();
        for ($s = 0; $s < count($progress); $s++) {            
            array_push($repoIDS, $progress[$s]->id_activity);            
        }         
                                             
        $list_member = Auth::user()->get();
        $profile = User::where('id',$id)->get();        
        $path = 'Project';       
        $arr = array();        

        foreach($data as $key => $dt){            
            foreach(json_decode($dt->id_user) as $ind => $dex){
                array_push($arr,$dex);            
            }
        }  

        $graph = User::select('photo','id','initial')->whereIn('id',$arr)->get();
        $countActivity = AM::select('id_project', DB::raw('COUNT(id_project) as jml_act'))
                            ->groupBy('id_project')
                            ->get();
        
        return view('backoffice.project.index', compact('data','list_member','activities','graph',
                                                        'path','profile','progress','repoIDS',
                                                        'countActivity','role'));
    }

    public function index_team()
    {        
        $id = Auth::user()->id;
        $data = ProjectModel::all();                
        Auth::user()->role == "admin" ? $role = "admin" : $role = "team";                        
        
        $progress = ProgressModel::all();                         
        $list_member = Auth::user()->get();
        $activities = AM::all();        
        $profile = User::where('id',$id)->get();        
        $path = 'Project';       
        $arr = array();
        $repoIDS = array();
        
        for ($s = 0; $s < count($progress); $s++) {            
            array_push($repoIDS, $progress[$s]->id_activity);            
        } 

        foreach($data as $key => $dt){            
            foreach(json_decode($dt->id_user) as $ind => $dex){
                array_push($arr,$dex);            
            }
        }                        
        $graph = User::select('photo','id','initial')->whereIn('id',$arr)->get();      
        $countActivity = AM::select('id_project', DB::raw('COUNT(id_project) as jml_act'))
                            ->groupBy('id_project')
                            ->get();                      

        return view('backoffice.project.index', compact('data','list_member','activities','graph','path',
                                                        'repoIDS','progress','profile','countActivity',
                                                        'role'));
    }

    public function store(Request $request)
    {
        $project = new ProjectModel;

        if(! empty($request->id_user)){
            $member = json_encode($request->id_user);
        }else{
            $member = null;
        }             
        
        if(! empty($request->file('file_attach'))){

            $validator = Validator::make($request->all(), [
                'file_attach'   => 'mimes:doc,pdf,docx,zip|max:2048'
            ]);   
            
            if($validator->fails()){                
                alert()->error('Data not saved', 'data does not match!');
                return redirect()->back();                
            }else{                
                $file = $request->file('file_attach');
                $nama_file = rand() . '.' . $file->getClientOriginalExtension();       
                $file->move(public_path('uploads/'), $nama_file);    
            }

        }else{
            $nama_file = null;
        }

        $project->nama = $request->nama;
        $project->deadline = $request->deadline;
        $project->deskripsi = $request->deskripsi;
        $project->file_attach = $nama_file;
        $project->id_user = $member;
        $project->created_by = Auth::user()->id;     

        $project->save();
        alert()->success('Data saved', 'Successfully Add Data Project!');
        return redirect()->back();        
    }

    public function update(Request $request, $id)
    {
        $project = ProjectModel::findOrFail($id);

        if(! empty($request->id_user)){
            $member = json_encode($request->id_user);
        }else{
            $member = null;
        }             
        
        if(! empty($request->file('file_attach'))){

            $validator = Validator::make($request->all(), [
                'file_attach'   => 'mimes:doc,pdf,docx,zip|max:2048'
            ]);   
            
            if($validator->fails()){                
                alert()->error('Data not saved', 'data does not match!');
                return redirect()->back();     
            }else{                
                $file = $request->file('file_attach');
                $nama_file = rand() . '.' . $file->getClientOriginalExtension();       
                $file->move(public_path('uploads/'), $nama_file); 
                $project->file_attach = $nama_file;
            }

        }

        $project->nama = $request->nama;
        $project->deadline = $request->deadline;
        $project->deskripsi = $request->deskripsi;
        $project->id_user = $member;

        $project->save();
        alert()->success('Data saved', 'Successfully Change Data Project!');
        return redirect()->back();        
    }

    public function destroy($id)
    {
        $project = ProjectModel::findOrFail($id);
        $myFile = $project->file_attach;        

        if(isset($myFile) && file_exists('uploads/'.$myFile)){            
            unlink('uploads/'.$myFile);
        }

        $project->delete();
        alert()->success('Deleted', 'Successfully Delete Data Project!');
        return redirect()->back();        
    }
}
