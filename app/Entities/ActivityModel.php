<?php

namespace App\Entities;
use App\Entities\ProjectModel;

use Illuminate\Database\Eloquent\Model;

class ActivityModel extends Model
{
    protected $table = 'activity';
    protected $fillable = [
        'id_project','activity','task','deadline'
    ];

    public function project()
    {
        return $this->belongsTo(ProjectModel::class, 'id_project', 'id');
    }
}
