<?php

namespace App\Entities;
use App\Entities\ActivityModel;

use Illuminate\Database\Eloquent\Model;

class ProgressModel extends Model
{
    protected $table = 'progress';
    protected $fillable = [
        'id_activity','task_done','status'
    ];

    public function activity()
    {
        return $this->belongsTo(ActivityModel::class, 'id_activity', 'id');
    }
}
