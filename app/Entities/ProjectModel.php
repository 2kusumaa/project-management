<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\User;
use App\Entities\ActivityModel;

class ProjectModel extends Model
{
    protected $table = 'project';
    protected $fillable = [
        'id_user','nama','deadline','deskripsi','file_attach','created_by'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function activity()
    {
        return $this->hasMany('App\Entities\ActivityModel', 'id_project', 'id');        
    }
}
