<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsTeam;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Landing Page 
Route::get('/', function () {
    return view('auth/login');
});

//Dashboard Page
// Route::resource('dashboard','V1\DashboardController');

Auth::routes();

//User (Admin or Team)'s space
Route::group(['namespace'=>'v1'], function(){ 
    Route::get('/profile', 'HomeController@index')->name('user.profile');
    Route::post('/profile/update-profile/{id}', 'HomeController@update')->name('user.update.profile');
});

//Admin (Project Manager)'s Space 
Route::group(['prefix'=>'admin', 'middleware'=> IsAdmin::class], function(){    

    Route::group(['namespace'=>'v1'], function(){        
        Route::post('/users/add-user', 'HomeController@storeUser')->name('user.store');
        Route::get('/users/data', 'HomeController@indexListUsers')->name('user.index');
        Route::post('/users/update-user/{id}', 'HomeController@updateUser')->name('user.update');
        Route::get('/users/delete-user/{id}', 'HomeController@destroyUser')->name('user.delete');
        Route::get('/dashboard', 'HomeController@admin')->name('dashboard.admin');

        Route::get('/project', 'ProjectController@index')->name('admin.project');
        Route::post('/add-project', 'ProjectController@store')->name('admin.add.project');
        Route::get('/detail-project/{id}', 'ProjectController@show')->name('admin.detail.project');
        Route::post('/update-project/{id}', 'ProjectController@update')->name('admin.update.project');
        Route::get('/delete-project/{id}', 'ProjectController@destroy')->name('admin.delete.project');
        
        Route::post('/add-activity', 'ActivityController@store')->name('admin.add.activity');
        Route::get('/delete-activity/{id}', 'ActivityController@destroy')->name('admin.delete.activity');

    });
});

//Team's Space
Route::group(['prefix'=>'team', 'middleware'=> IsTeam::class], function(){
    Route::group(['namespace'=>'v1'], function(){        
        
        Route::get('/dashboard', 'HomeController@team')->name('dashboard.team');
        Route::get('/project', 'ProjectController@index_team')->name('team.project');

        Route::post('/add-activity', 'ActivityController@store')->name('team.add.activity');
        Route::get('/delete-activity/{id}', 'ActivityController@destroy')->name('team.delete.activity');
    });
});
