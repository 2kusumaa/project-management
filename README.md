# Project Managemen Divisi Teknologi Informsi  - PT PAL (Persero)

## First Usage

1. Run command `composer update` in your workspace terminal.
2. Run command `php artisan migrate:refresh` in your workspace terminal.
3. Run command `php artisan db:seed` in your workspace terminal.
4. Run command `php artisan storage:link` in your workspace terminal.

## How to contribute
- (**IMPORTANT**) 
- Clone this repo
- Create your own branch for feature you want to add (suggestion for branch name : `feature/your_feature_name` or `hotfix/your_bug_to_solve`)
- Push and add a **merge request** to `development` branch OR merge request to `master` branch if it's an hotfix.
- DONT FORGET to add necessary information into `readme.md`
