@extends('layouts.main')
@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users Data</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-exclamation-triangle"></i>
            Data
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">   
          <div class="mb-4">
          </div>                                
          <div class="col-10 my-10 mx-auto">
            <button type="button" class="btn btn-sm btn-primary mb-4 mr-2" data-toggle="modal" data-target="#modal-add-user"><i class="fas fa-plus"></i> Add user</button> 
            <button type="button" class="btn btn-sm btn-info mb-4 btn-search"><i class="fas fa-search"></i> Search user</button> 
            <input type="text" class="form-control fuzzy-search search-input" placeholder="Search user data..">
            @include('backoffice.user.components.modal-add')
          </div> <br>          
          <div class="" id="user-list">
            <div class="row list">
            @foreach($data_users as $key => $user)
            <div class="callout callout-danger col-md-10 mx-auto">    
              <div class="row">
                <div class="col-md-8 title">
                  <h5># {{ $user->name }} <b>as</b> {{ ($user->role == 'pm') ? "project manager" : $user->role }} </h5>
                  <p>
                    <b>Initial: </b> {{ $user->initial }}<br>
                    <b>Email:</b> {{ $user->email }}<br>
                    <b>Phone:</b> {{ $user->phone }}<br>
                  </p>
                  <button class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-edit-user{{$user->id}}"><i class="fas fa-pencil-alt"></i> Edit</button>                     
                    @include('backoffice.user.components.modal-edit')                  
                  <button class="btn btn-sm btn-danger" onclick="alertDelUser({{$user->id}})"><i class="fas fa-trash"></i> Delete</button>
                </div>
                <div class="col-md-4">
                  <img src="{{ URL::to('/')}}/uploads/photo/{{ $user->photo }}" class="img-thumbnail elevation-2 float-right" width="120" height="125" alt="User Photo">
                </div>
              </div>                        
            </div>                      
            @endforeach    
              @foreach($data_users as $key => $user)
              @endforeach          
            </div>
          </div>          
        </div>        
        
        <!-- /.card-body -->        
      </div>
    </section>
    <!-- /.content -->        
@endsection
<!-- jQuery -->                  

@include('backoffice.user.components.js')   
<!------------->

