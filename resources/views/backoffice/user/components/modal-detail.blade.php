<!----FORM ADD DATA -->
<div class="modal fade" id="modal-detail-project{{$value->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Detail-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/add-activity')}}" class="form-horizontal formdetail{{$key}}" method="post" enctype="multipart/form-data">                    
                    @csrf
                    <div class="callout callout-info">
                        <h5><i class="icon fas fa-info"></i> Desc</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-8">
                                <h5>{{ $value->nama }}</h5>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-sm btn-warning float-right">{{\Carbon\Carbon::parse($value->deadline)->format('j F Y')}}</button>
                            </div>
                        </div>
                        <p>{{ $value->deskripsi }} <br> Project Files : <br> <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i>{{ $value->file_attach }}</a></p>                        
                    </div>  

                    @foreach($activities as $ind => $val)
                        @if($val->id_project == $value->id)
                        @php $dex = $ind; @endphp
                        <div class="col-12" id="accordion">
                            <div class="card card-primary card-outline">
                                <a class="d-block w-100" data-toggle="collapse" href="#collapse{{$ind}}">
                                    <div class="card-header">
                                        <h4 class="card-title w-100">
                                            <b>#</b> {{ $val->activity }}
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapse{{$ind}}" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="hidden" name="sid_act" value="{{ $val->id }}">
                                            <div id="input_tasks{{$key}}{{$ind}}">
                                                <input type="text" name="task" id="task{{$key}}{{$ind}}0" class="form-control mb-1 task{{$key}}{{$ind}}" placeholder="input task..">
                                            </div>
                                            <button type="button" onclick="addTask({{$key}},{{$ind}})" class="btn btn-sm btn-primary float-right btn-add-task{{$key}}{{$ind}}">Add more task</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach

                    <div class="col-md-12 mb-2" id="add_activity{{$key}}"></div>
                    <input type="hidden" name="id_project" id="id_project{{$key}}" value="{{ $value->id }}">                    
                    <input type="hidden" name="activity[]" id="repoActivity{{$key}}">                    

                    <button type="button" id="add_activity_btn{{$key}}" onClick="addActivity({{$key}})" class="btn btn-sm btn-secondary col-md-12">
                            <i class="fas fa-plus"></i> &nbsp add activity
                    </button>                
            </div>
            <div class="modal-footer">
                <button type="button" onclick="sumbitDataActivity({{$key}},{{$dex}})" id="btn_submit_activity" class="btn btn-success">Save</button>
            </div>
            </form>
        </div>
    </div>