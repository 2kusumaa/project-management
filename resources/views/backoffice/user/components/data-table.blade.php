<table id="data-table-project-s1" class="table table-striped" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">
    <thead>
        <tr style="background-color:#001f3f; color:white; font-size:13px;">
            <th width="3%">#</th>
            <th>Project Name</th>           
            <th>Member</th>
            <th>Project Progress</th>  
            <th>Deadline</th> 
            <th width="11%"><center><i class="fas fa-tools"></i></center></th>          
        </tr>
    </thead>
    <tbody style="font-size:16px;">
        @foreach($data as $key => $value)    
            <tr>
                <td>#</td>
                <td><b>{{ $value->nama }}</b> <br> 
                    <small>created at : <b>{{\Carbon\Carbon::parse($value->created_at)->format('j F Y')}}                        
                    </b> </small> <br>                    
                </td>            
                <td>
                @foreach(json_decode($value->id_user) as $dt)
                    {{ $dt }}
                @endforeach
                </td>
                <td>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                        </div>
                    </div>
                    <small>
                        57% Complete
                    </small>
                </td>
                <td>
                    <span class="badge badge-warning">{{\Carbon\Carbon::parse($value->deadline)->format('j F Y')}}</span>
                </td>
                <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-edit-project{{$value->id}}">Edit</a>                    
                    <a class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-detail-project{{$value->id}}">Manage</a>                    
                    <a onclick="alertFuncToDelete({{$value->id}})" class="btn btn-sm btn-danger">Delete</a>                    
                </div>
                </td>
              @include('backoffice.project.components.modal-edit')       
            </tr>        
            @include('backoffice.project.components.modal-detail')       
        @endforeach
    </tbody>
</table>