<script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/list.min.js') }}"></script>

<script>
$(document).ready(function(){
    $('.search-input').css('display', 'none');

    $('.btn-search').click(function(){
      if($('.search-input:hidden').length != 0){
        $('.search-input').css('display', 'block');
      }else{
        $('.search-input').css('display', 'none');
      }
    })

    var options = {
        valueNames: [ 'title', 'desc' ]
    };

    var userList = new List('user-list', options);

    $(".fuzzy-search").keyup(function(){        
      userList.search($(this).val());         
    });    
});

</script>
<script>  
  $(function () {
    $("#data-table-project-s1").DataTable({
      "paging": true,

    });

  });    
  
  /** Delete warning */
  function alertFuncToDelete(id) {
        Swal.fire({
            title: 'Apakah anda yakin ?',
            icon: 'warning',
            text : "Data project ini akan di hapus beserta file!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            Swal.fire(
            'Deleted!',
            'Your data has been deleted.',
            'success'
            ).then(function() {
                window.location.href = "/admin/delete-project/"+id;
            });
        }
    });
  }
  
  function alertDelUser(id) {
        Swal.fire({
            title: 'Apakah anda yakin ?',
            icon: 'warning',
            text : "Data user ini akan di hapus!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            Swal.fire(
            'Deleted!',
            'Your data has been deleted.',
            'success'
            ).then(function() {
                window.location.href = "/admin/users/delete-user/"+id;
            });
        }
    });
  }

  // During add activity event
  function addActivity(key) {            
    let cont =  $('.activity'+key).length;
    let index = cont + 1;
    
    let input = document.createElement("input");
      input.setAttribute("type", "text");      
      input.setAttribute("id", "activity"+key+index);      
      input.setAttribute("name", "activity[]");      
      input.setAttribute("class", "form-control mb-2 activity"+key);
      input.setAttribute("placeholder", "add activity");      
      document.getElementById("add_activity"+key).appendChild(input);                       
  }

  // During add activity event
  function addTask(key,ind) {            
    let cont =  $('.task'+key+ind).length;
    let index = cont++;
    
    let input = document.createElement("input");
      input.setAttribute("type", "text");      
      input.setAttribute("id", "task"+key+ind+index);      
      input.setAttribute("name", "task");      
      input.setAttribute("class", "form-control mb-1 task"+key+ind);
      input.setAttribute("placeholder", "input task..");      
      document.getElementById("input_tasks"+key+ind).appendChild(input);                       
  }

  // Submit data activity
  function sumbitDataActivity(key){        
    let cont_task =  $("input[name=task]").length; //
    let index_task = cont_task - 3; //
    
    let cont =  $('.activity'+key).length;
    let index = cont++;

    let arr = []; 
    let arr_task = [];   //
    let data1 = $("#id_project"+key).val();      

      for (let n = 0; n <= index_task; n++) {
          let val = $("#task"+key+n+n).val();
          console.log(val);
          if($("#task"+key+index_task+n).val() != ""){
          arr_task.push(val);
        }        
      }             

      for (let i = 1; i <= index; i++) {
        let val = $("#activity"+key+i).val();
        if($("#activity"+key+i).val() != ""){
          arr.push(val);
        }        
      }

    $("#repoActivity"+key).val(arr);
    // $("input[name=task]").val(arr_task); //
    
    // $(".formdetail"+key).submit();        
  }  

</script>