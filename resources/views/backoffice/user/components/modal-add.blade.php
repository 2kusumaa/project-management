<!----FORM ADD DATA -->
<div class="modal fade" id="modal-add-user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Add-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/users/add-user/')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Name : </label>
                                <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                            </div>
                            <div class="col-md-4">
                                <label for="">Initial : </label>
                                <input type="text" class="form-control" name="initial" placeholder="Enter initial user">
                            </div>                            
                        </div>                        
                    </div>
                    <div class="form-group">  
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Email : </label>
                                <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                            </div>
                            <div class="col-md-6">
                                <label for="">Phone (+62) : </label>
                                <input type="number" class="form-control" name="phone">
                            </div>
                        </div>                                          
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">User As:</label>
                                <select name="role" class="form-control" required>
                                    <option value="admin">Admin</option>
                                    <option value="pm">Project Manager</option>
                                    <option value="team">Team</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="input the password.." required>
                            </div>
                        </div>                        
                    </div>
                    <label for="">Photo Profile</label><br>
                    <div class="form-group row">                        
                        <div class="col-sm-4">
                          <input type="file" name="photo" class="form-control-file" id="inputPhoto">                          
                        </div>
                    </div>                                                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger"><i class="fa fa-arrow-right"></i> Save</button>
            </div>                        
        </form>
        </div>
    </div>
</div>