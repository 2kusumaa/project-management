<!----FORM ADD DATA -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-edit-user{{$user->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Edit-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('admin/users/update-user/'.$user->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">                    
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Name : </label>
                                <input type="text" class="form-control" value="{{ $user->name }}" name="name" placeholder="Enter name" required>
                            </div>
                            <div class="col-md-4">
                                <label for="">Initial : </label>
                                <input type="text" class="form-control" value="{{ $user->initial }}" name="initial" placeholder="Enter initial user">
                            </div>                            
                        </div>                        
                    </div>
                    <div class="form-group">  
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Email : </label>
                                <input type="email" class="form-control" value="{{ $user->email }}" name="email" placeholder="Enter email" required>
                            </div>
                            <div class="col-md-6">
                                <label for="">Phone (+62) : </label>
                                <input type="number" class="form-control" value="{{ $user->phone }}" name="phone">
                            </div>
                        </div>                                          
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">User As:</label>
                                <select name="role" class="form-control" required>
                                @if($user->role == 'admin')                            
                                    <option value="admin" selected="selected">Admin</option>
                                    <option value="pm">Project Manager</option>
                                    <option value="team">Team</option>
                                @elseif($user->role == 'pm')
                                    <option value="admin">Admin</option>
                                    <option value="pm" selected="selected">Project Manager</option>
                                    <option value="team">Team</option>
                                @elseif($user->role == 'team')
                                    <option value="admin">Admin</option>
                                    <option value="pm">Project Manager</option>
                                    <option value="team" selected="selected">Team</option>
                                @endif
                                </select>
                            </div>                                                        
                            <div class="col-md-6">
                                <!-- <label for="">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="input the password.." required> -->
                            </div>
                        </div>
                    </div>
                    <label for="">Photo Profile</label><br>
                    <div class="form-group row">                        
                        <div class="col-sm-4">
                          <input type="file" name="photo" class="form-control-file">
                          <small>*Current file -- {{ $user->photo }}</small>
                        </div>
                    </div>
                    <div class="form-group float-right">
                        <button type="button" class="btn btn-default mr-1" data-dismiss="modal">Close</button>                                                                                    
                        <button type="submit" class="btn btn-danger"><i class="fa fa-arrow-right"></i> Save</button>            
                    </div>                                           
                </form>
            </div>                        
        </div>
    </div>
</div>