<!----FORM ADD DATA -->
<div class="modal fade" id="modal-edit-project{{$value->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Edit-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/update-project/'.$value->id)}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                    @csrf
                    <div class="form-group">
                        <label for="">Project Name : </label>
                        <input type="text" class="form-control" value="{{ $value->nama }}" name="nama" placeholder="Enter project name" required>
                    </div>

                    <div class="form-group">
                        <label for="">Deadline : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                            </div>
                            <input type="date" class="form-control" value="{{ $value->deadline }}" name="deadline" required>
                        </div>
                    </div>                    
                    
                    <div class="form-group">
                        <label>Add Member</label>                                                
                        <div class="select2-purple">
                        <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="id_user[]">
                            @foreach($list_member as $member)
                                @if( in_array($member->id,json_decode($value->id_user)))
                                    <option selected="selected" value="{{ $member->id }}">{{ $member->name }}</option>
                                @else
                                    <option value="{{ $member->id }}">{{ $member->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="deskripsi" placeholder="Project description">{{ $value->deskripsi }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Upload File</label>
                        <input type="file" name="file_attach" class="form-control-file">
                        @if($value->file_attach != null)
                            <small><i>*Current file: </i><b>{{ $value->file_attach }}</b></small><br>
                            <small><i><b>Upload file to change current file</i></b></small><br>
                        @else
                            <small><b><i>*No file yet</i></b></small>
                        @endif
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger"><i class="fa fa-arrow-right"></i> Save</button>
            </div>                        
        </form>
        </div>
    </div>
</div>