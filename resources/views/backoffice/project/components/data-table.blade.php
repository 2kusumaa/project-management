
<div class="table-responsive">
<table id="data-table-project" class="table table-striped" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">
    <thead>
        <tr style="background-color:#001f3f; color:white; font-size:13px;">
            <th width="3%">#</th>
            <th>Project Name</th>           
            <th>Member</th>
            <th>Project Progress</th>  
            <th>Deadline</th> 
            <th width="11%"><center><i class="fas fa-tools"></i></center></th>          
        </tr>
    </thead>
    <tbody style="font-size:16px;">
        @foreach($data as $key => $value)    
            @if(Auth::user()->id == $value->created_by || in_array(Auth::user()->id,json_decode($value->id_user)) || Auth::user()->role == 'admin')
            <tr>
                <td># <input type="hidden" class="idproject_" value="{{ $value->id }}"></td>
                <td><b>{{ $value->nama }}</b> <br> 
                    <small>created at : <b>{{\Carbon\Carbon::parse($value->created_at)->format('j F Y')}}                        
                    </b> </small> <br>                    
                </td>                            
                <td>                    
                    <ul class="list-inline">
                    @foreach($graph as $key => $pt)                                                
                            @php $count = 0; if($count == 3) break; @endphp                            
                            <li class="list-inline-item"> 
                                @if(in_array($pt->id,json_decode($value->id_user)))
                                    <div class="popup">
                                        <img alt="Avatar" class="table-avatar rounded-circle" onclick="toPopup({{ $pt->id }})" height="30px" width="30px" src="{{ URL::to('/')}}/uploads/photo/{{ $pt->photo }}">
                                        <span class="popuptext" id="myPopup{{ $pt->id }}">{{ $pt->initial }}</span>
                                    </div>
                                @endif
                            </li>                                                    
                            @php $count++; @endphp                        
                    @endforeach
                    </ul>
                </td>                
                <td>                               
                    <div class="progress progress-sm">                    
                        <div class="progress-bar-dt_{{$value->id}} bg-green" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        </div>                    
                    </div>                                        
                    <small class="jmlact_{{$value->id}}">
                        0
                    </small>                         
                </td>
                <td>
                    <span class="badge badge-warning">{{\Carbon\Carbon::parse($value->deadline)->format('j F Y')}}</span>
                </td>
                <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    @if(Auth::user()->role == "admin" || Auth::user()->role == "pm" )
                        <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-edit-project{{$value->id}}">Edit</a>                    
                    @endif
                    @if(Auth::user()->role == "pm" || Auth::user()->role == "team")
                        <a class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-detail-project{{$value->id}}">Manage</a>                    
                    @endif
                    @if(Auth::user()->role == "admin" || Auth::user()->role == "pm" )
                        <a onclick="alertFuncToDelete({{$value->id}})" class="btn btn-sm btn-danger">Delete</a>                    
                    @endif
                </div>
                </td>
              @include('backoffice.project.components.modal-edit')       
            </tr>
            @include('backoffice.project.components.modal-detail')       
            @endif        
        @endforeach
    </tbody>    
</table>    
</div>