<script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<script>  

  // GLOBAL VARIABLE 
  
  // END GLOBAL VARIABLE  
  window.onload = function() {
    
  ////// START PROGRESS BAR MODAL DETAIL ACTIVITY   
    let all_cb = $('input[type=checkbox]').length;       
    let ress = [];
    for (y = 0; y < all_cb; y++) { 
      //Push each element to the array      
      ress.push($('.cb').eq(y).val());
    }
    
    for (p = 0; p < ress.length; p++) {
      
      if(typeof ress[p] != 'undefined'){
        let angk_ = ress[p].split("$$");
        checklist(angk_[1]);                    
      }
    };
  ////// END PROGRESS BAR MODAL DETAIL ACTIVITY      

  }

  $(function () {
    $("#data-table-project").DataTable({
      "paging": true,

    });

  });
  
  /** Delete warning */
  function alertFuncToDelete(id) {
        Swal.fire({
            title: 'Apakah anda yakin ?',
            icon: 'warning',
            text : "Data project ini akan di hapus beserta file!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            Swal.fire(
            'Deleted!',
            'Your data has been deleted.',
            'success'
            ).then(function() {
                window.location.href = "/admin/delete-project/"+id;
            });
        }
    });
  }
  
  /** Delete warning */
  function alertFuncToDeleteActivity(id) {
        Swal.fire({
            title: 'Apakah anda yakin ?',
            icon: 'warning',
            text : "Data acvtivity ini akan di hapus!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            Swal.fire(
            'Deleted!',
            'Your data has been deleted.',
            'success'
            ).then(function() {
                window.location.href = "/admin/delete-activity/"+id;
            });
        }
    });
  }

  // During add activity event
  function addMoreTask(key) {            
    let cont =  $('input[name=inputtask'+key+']').length;
    let index = cont + 1;    
      
    let input_task = document.createElement("input");
      input_task.setAttribute("type", "text");                 
      input_task.setAttribute("name", "inputtask"+key);      
      input_task.setAttribute("class", "form-control mb-1 task"+key+index);
      input_task.setAttribute("placeholder", "Enter task..");      
      document.getElementsByClassName("add_task"+key)[0].appendChild(input_task);      
  }  

  function sumbitDataActivity(key) {
    let all_cb = $('input[type=checkbox]').length;           
    let cont =  $('input[name=inputtask'+key+']').length;
    let index = cont;    

    let result = [];
    for (i = 0; i < index; i++) { 
      //Push each element to the array
      if($('input[name=inputtask'+key+']').eq(i).val() != "") {
        result.push($('input[name=inputtask'+key+']').eq(i).val());
      }
    }

    let array_taskDone = [];  
    for (q = 0; q < all_cb; q++) {
      //Push each element cb to the array
      array_taskDone.push($('.cb').eq(q).val());
    } 
    
    $('#repoTask'+key).val(result);
    $(".repo-taskDone").val(array_taskDone);
    $('.formdetail'+key).submit();

  }    


  $(document).ready(function() {
    $('input[type=checkbox]').change(function() {      

      if (this.checked) {
        $(this).next(".label-text").css("text-decoration-line", "line-through");                                                 
          if( $(this).val().substring(0, $(this).val().length - 4) != "done"){                   
            $(this).val( $(this).val().substring(($(this).val().length) - 4, 0) ); // value wihtout xxxx      
            $(this).val($(this).val()+'done'); 
          }            
      } else {
        $(this).next(".label-text").css("text-decoration-line", "none");                     
          $(this).val( $(this).val().substring(($(this).val().length) - 4, 0) );            
          $(this).val($(this).val()+'xxxx');  
      }           

    });
  });

  function checklist(sid) {
    let count = $('input[name^=customCheckbox'+sid+']').length;  
    let min = $('input[name^=customCheckbox'+sid+']:checked').length;

    let sum = parseInt(count) - parseInt(min);
    let persentage = 100/parseInt(count);  
        

    $('.progress-bar'+sid).css('width', persentage*parseInt(min)+'%');       
    $('.perComplete'+sid).html(Math.floor(persentage*parseInt(min))+' % Complete');     

    let v = $('input[name^=customCheckbox'+sid+']').attr('name').split("-"); // id project
        
    getCountProjectProgress(v[1], sid, persentage*parseInt(min), parseInt(min));    
  }

  let arr_res = [];
  function getCountProjectProgress(ids, sid, progres, varr_)
  {            
    arr_res.push({
                  idp: ids,
                  ida: sid,
                  prog: progres,
                  varr: varr_ 
                });    
                console.log(ids);
    let jct = $(".jmlact_"+ids).attr('class').split("_");   
    
    let sums = 0;    
    
    const y = arr_res.filter((obj) => obj.idp == jct[1]).map((obj) => obj.prog);    
    const z = arr_res.filter((obj) => obj.idp == jct[1]).map((obj) => obj.prog).length;    
    
    for( var el in y ) {
      if( y.hasOwnProperty( el ) ) {
        sums += parseFloat( y[el] );
      }      
    }            

    // console.log(sums);
    
    if(sums == 0){
      $(".jmlact_"+ids).text(0);
      $(".progress-bar-dt_"+ids).css('width', 0+'%');  
    }else{
      $(".jmlact_"+ids).text(Math.floor(sums/z)+'% Complete');
      $(".progress-bar-dt_"+ids).css('width', sums/z+'%');  
    }
  }

</script>