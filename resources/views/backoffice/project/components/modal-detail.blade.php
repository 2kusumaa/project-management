<!----FORM ADD DATA -->
<div class="modal fade" id="modal-detail-project{{$value->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Detail-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">                
                <form action="{{url($role.'/add-activity')}}" class="form-horizontal formdetail{{$value->id}}" method="post" enctype="multipart/form-data">                    
                    @csrf
                    <div class="callout callout-info">
                        <h5><i class="icon fas fa-info"></i> Desc</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-8">
                                <h5>{{ $value->nama }}</h5>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-sm btn-warning float-right">{{\Carbon\Carbon::parse($value->deadline)->format('j F Y')}}</button>
                            </div>
                        </div>
                        <p>{{ $value->deskripsi }} <br> Project Files : <br> <a target="_blank" href="{{asset('uploads/'.$value->file_attach)}}" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i>{{ $value->file_attach }}</a></p>                        
                    </div>  
                                        
                    @foreach($activities as $ind => $val)
                        @if($val->id_project == $value->id)                        
                        <div class="col-12" id="accordion">
                            <div class="card card-primary card-outline">                            
                                <a class="d-block w-100" data-toggle="collapse" href="#collapse{{$ind}}">
                                    <div class="card-header">
                                        <div class="row mb-2">
                                            <div class="col-md-10">
                                                <h4 class="card-title w-100">
                                                    <div class="activityProject{{$value->id}}"><b>#</b> {{ $val->activity }}<br>
                                                    @if($val->deadline < \Carbon\Carbon::now())
                                                        <span class="badge bg-danger mt-2">Deadline: {{\Carbon\Carbon::parse($val->deadline)->format('j F Y')}}</span><br>                                                    
                                                    @else
                                                        <span class="badge bg-warning mt-2">Deadline: {{\Carbon\Carbon::parse($val->deadline)->format('j F Y')}}</span><br>                                                    
                                                    @endif
                                                    </div>
                                                </h4>
                                            </div>
                                            <div class="col-md-2">
                                                @if(Auth::user()->role == "pm")
                                                <button type="button" onclick="alertFuncToDeleteActivity({{$val->id}})" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                @endif
                                            </div>                                                                                        
                                        </div>                                                                            
                                            <div class="progress progress-sm">
                                                <div class="progress-bar bg-green progress-bar{{$val->id}}" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                </div>
                                            </div>
                                            <small class="perComplete{{$val->id}}" name="perComplete{{$value->id}}{{$val->id}}">                                                
                                                0% Complete 
                                            </small>                                                                                    
                                    </div>
                                </a>                                               
                                <div id="collapse{{$ind}}" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                    @if(in_array($val->id,$repoIDS))                                                                        
                                        @foreach($progress as $keys => $prog)
                                            @if($prog->id_activity == $val->id)
                                                <div class="custom-control custom-checkbox">
                                                    @if($prog->status == "done")
                                                        <input class="custom-control-input cb" type="checkbox" checked="checked" onchange="checklist({{ $val->id }})" name="customCheckbox{{$val->id}}{{$keys}}-{{$value->id}}" id="customCheckbox{{$val->id}}{{$keys}}" value="{{ $prog->task_done }}$${{ $val->id }}$$done">
                                                        <label for="customCheckbox{{$val->id}}{{$keys}}" style="text-decoration: line-through;" class="label-text custom-control-label">{{ $prog->task_done }}</label>
                                                    @else
                                                        <input class="custom-control-input cb" type="checkbox" onchange="checklist({{ $val->id }})" name="customCheckbox{{$val->id}}{{$keys}}-{{$value->id}}" id="customCheckbox{{$val->id}}{{$keys}}" value="{{ $prog->task_done }}$${{ $val->id }}$$xxxx">
                                                        <label for="customCheckbox{{$val->id}}{{$keys}}" class="label-text lbl{{ $val->id }} custom-control-label">{{ $prog->task_done }}</label>
                                                    @endif
                                                </div>                         
                                            @endif                         
                                        @endforeach                                                                            
                                    @else      
                                        @foreach(json_decode($val->task) as $x => $sub)                                                                                                                                                                                                                   
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input cb" type="checkbox" onchange="checklist({{ $val->id }})" name="customCheckbox{{$val->id}}{{$x}}-{{$value->id}}" id="customCheckbox{{$val->id}}{{$x}}" value="{{ $sub }}$${{ $val->id }}$$xxxx">
                                                <label for="customCheckbox{{$val->id}}{{$x}}" class="label-text custom-control-label lbl{{ $val->id }}">{{ $sub }}</label>
                                            </div>                                                  
                                        @endforeach           
                                    @endif                   
                                    <input type="hidden" class="sid_act" value="{{ $val->id }}">                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    
                    @if(Auth::user()->role == 'pm')
                    <div class="col-md-12 mb-2">
                        <div class="form-group">
                            <label for="">Aktivities</label>
                            <div class="add_activity{{$value->id}}">
                                <input type="text" name="activity" class="form-control mb-2" placeholder="Enter activity.." required>                        
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="">Deadline</label>
                            <div class="add_deadline{{$value->id}}">
                                <input type="date" name="deadline" class="form-control mb-2" required>                        
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-12 mb-1">
                        <div class="form-group">
                            <label for="">Task</label>
                            <div class="add_task{{$value->id}}">
                                <input type="text" name="inputtask{{$value->id}}" class="form-control mb-1 task{{$value->id}}0" placeholder="Enter task.." required>                                
                            </div>
                            <div class="d-flex justify-content-center">                            
                            <button type="button" class="btn btn-sm btn-secondary m-auto add_task{{$value->id}}" onclick="addMoreTask({{ $value->id }})"><i class="fas fa-plus"></i> Add more task</button>                            
                            </div>
                        </div>                        
                    </div>
                    @endif
                    <input type="hidden" name="id_project" id="id_project{{$value->id}}" value="{{ $value->id }}">                                        
                    <input type="hidden" name="task[]" id="repoTask{{$value->id}}">     
                    <input type="hidden" class="repo-taskDone" name="taskDone[]" id="taskDone{{$value->id}}">                                   
            </div>            
            <div class="modal-footer">
                <button type="button" onclick="sumbitDataActivity({{$value->id}})" id="btn_submit_activity{{$value->id}}" class="btn btn-success"><i class="fas fa-share"></i> Save</button>
            </div>
            </form>            
        </div>
    </div>