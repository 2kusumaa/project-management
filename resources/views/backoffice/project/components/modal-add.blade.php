<!----FORM ADD DATA -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-edit"></i> Form Add-{{ $path }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin/add-project')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                    @csrf
                    <div class="form-group">
                        <label for="">Project Name: </label>
                        <input type="text" class="form-control" name="nama" placeholder="Enter project name" required>
                    </div>

                    <div class="form-group">
                        <label for="">Deadline : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                            </div>
                            <input type="date" class="form-control" name="deadline" required>
                        </div>
                    </div>                    
                    
                    <div class="form-group">
                        <label>Add Member</label>                                                
                        <div class="select2-purple">
                        <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="id_user[]">
                            @foreach($list_member as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="deskripsi" placeholder="Project description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Upload File</label>
                        <input type="file" name="file_attach" class="form-control-file">
                    </div>                    


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger"><i class="fa fa-arrow-right"></i> Save</button>
            </div>
        </form>
        </div>
    </div>
</div>