@extends('layouts.main')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        @if(Auth::user()->role == "pm")
          <h1>
            {{ "Project Manager" }} - {{ Auth::user()->name }}
          </h1>
        @elseif(Auth::user()->role == "team")
          <h1>
            {{ "Team" }} - {{ Auth::user()->name }}
          </h1>
        @else
          <h1>
            {{ "Admin" }} - {{ Auth::user()->name }}
          </h1>
        @endif
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">{{ $path }}</li>
          </ol>
        </div>
      </div>
  </div>
</section>
<section class="content">
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">        
        <div class="card-header">
              <h1 class="card-title" style="font-size:130%"><i class="fas fa-building"></i> &nbsp Data Project </h1>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>        
        <div class="card-body">
          <div class="col-md-7" style="margin-left:1%">
              @include('shared.flashmessage')
          </div>
          <div class="col-md-3" style="margin-left:1%;">
          @if(Auth::user()->role == "admin" || Auth::user()->role == "pm")
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
              <i class="fas fa-plus"></i> Tambah data
            </button>
          @endif
          </div>                                       
              @include('backoffice.project.components.modal-add')       
        </div><br>        
        <!-- datatable -->
          @include('backoffice.project.components.data-table')   
        <!-- endtable -->
      </div>
    </div>
  </div>
</div>
</section>
@endsection
<!-- jQuery -->

@include('backoffice.project.components.js')   
<!------------->

