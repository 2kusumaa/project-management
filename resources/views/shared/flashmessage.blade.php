@if ($message = Session::get('success'))
<div id="notice" class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <i class="fas fa-check"></i> &nbsp {{ $message }}
</div>
@endif

@if ($message = Session::get('error'))
<div id="notice" class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <i class="fas fa-exclamation-triangle"></i> &nbsp {{ $message }}
</div>
@endif

@if ($message = Session::get('warning'))
<div id="notice" class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <i class="fas fa-exclamation-triangle"></i> &nbsp {{ $message }}
</div>
@endif

@if ($message = Session::get('info'))
<div id="notice" class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
</div>
@endif
